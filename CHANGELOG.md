## 0.10.0
* `~ocelot` command (for now just with a reference)
* `~uk` command to search by russian criminal code articles
* `~base64 decode` and `~base64 encode` commands
* `~zalgo` command to make the world around more chthonic
* `~vc` command for local GTA fans
* `~duck` command which give an access to DuckDuckGo instant answers API
* Added the possibility to cancel `~mq` with `~cancel` command

## 0.9.1
* Fixed `~wiki` output in the cases when the article snippet contains
multiple whitespaces in the middle.

## 0.9.0
* Added support for russian language to the `~rhyme` command
* Added `~anagram` command
* Fixed bugs in `~cite` action
* Added `~tell` and `~gitlab` aliases
* Added info about domain provider to the `~geo` output
* Added `~time` by hostname

## 0.8.0
* Added an alias `~locate` for the `~geo` command
* Support for the cases when user accidentally puts whitespace before the command
* Upgraded `~?` to answer on all unrecognized commands that ends with a `?`
* Removed one-line quotes addition, and replaced it with fetching quotes by ID
* Source code refactoring

## 0.7.0
* Added `~geo` command for getting geo location by hostname

## 0.6.1
* Fixed `~anime` and `~hook` commands

## 0.6.0
* Added arrow modifier to the `~title` command (now you can do things like `~title ^`)
* Upgraded `~coin` command to understand constructions like `~btc to rub`
* Added `~turbofish`
* Changed versioning scheme to be more in the spirit of semver
* Refactoring & bugfixes (geolocation, command names conflicts)

## 0.5.4
* Added redirection to corresponding brote commands in case of misdirection
* Added geolocation by user IP to the `~weather` command 
* Allowed to `~tt` and `~t9` custom given phrases
* Excluded user nicknames from `~tt` command conversion
* Refactoring (annotation-based commands registration) & fixes (commands case-sensitivity)

## 0.5.3
* Added `~nocode`, `~neko` and `~swag` commands
* Updated `~hook` command to be more concise
* Added integration to GitLab CI
* Updated dependecies

## 0.5.2
* A lot of stuff
