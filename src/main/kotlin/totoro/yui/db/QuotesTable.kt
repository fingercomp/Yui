package totoro.yui.db

import java.sql.Connection
import java.sql.ResultSet

class QuotesTable(connection: Connection) : Table(connection, "quotes") {

    fun init() {
        update("create table if not exists $table (text string);")
    }

    fun add(quote: Quote): Long? {
        // escape single quotes
        val text = quote.text.replace("'", "''")
        // execute select query
        return insert("insert into $table values('$text');").getOrNull(0)
    }

    fun random(): Quote? {
        return firstFrom(query("select rowid, * from $table order by random() limit 1;"))
    }

    fun get(id: Long): Quote? {
        return firstFrom(query("select rowid, * from $table where rowid = $id;"))
    }

    /**
     * Reads all Quote objects from the result set, and the closes it.
     * Returns the first quote from the list, if any.
     */
    private fun firstFrom(result: ResultSet): Quote? {
        val quotes: MutableList<Quote> = mutableListOf()
        while (result.next()) {
            quotes.add(Quote(
                    result.getLong(1),
                    result.getString("text")
            ))
        }
        result.close()
        return quotes.getOrNull(0)
    }
}
