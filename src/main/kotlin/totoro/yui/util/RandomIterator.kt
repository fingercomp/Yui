package totoro.yui.util

import totoro.yui.Yui

class RandomIterator<T>(private val collection: List<T>): Iterator<T>, Iterable<T> {

    private val prime: Long = 2147483647
    private var last = Yui.Random.nextInt(collection.size)
    private var taken = 0

    init {
        if (collection.size > prime) {
            throw IllegalArgumentException("Collection size must be equal or less than $prime!")
        }
    }

    override fun hasNext(): Boolean {
        return taken < collection.size
    }

    override fun next(): T {
        val next = ((last + prime) % collection.size).toInt()
        last = next
        taken++
        return collection[next]
    }

    override fun iterator(): Iterator<T> {
        return this
    }
}
