package totoro.yui.util

object StringHelper {
    private val multipleWhitespacesPattern = "\\s+".toRegex()

    fun deWhitespace(value: String?): String? {
        return value?.replace(multipleWhitespacesPattern, " ")
    }
}
