package totoro.yui.util.api

import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import totoro.yui.util.api.data.DuckResult

object DuckDuckGo {
    fun search(query: String, success: (DuckResult) -> Unit, failure: () -> Unit) {
        "https://api.duckduckgo.com/?q=$query&format=json&no_html=1&no_redirect=1&skip_disambig=1"
                .httpGet()
                .header(
                        "accept" to "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8",
                        "accept-language" to "en-US,en;q=0.7,ru;q=0.3",
                        "cache-control" to "max-age=0",
                        "upgrade-insecure-requests" to "1",
                        "user-agent" to "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36"
                )
                .responseString { _, _, result ->
            when (result) {
                is Result.Failure -> {
                    failure()
                }
                is Result.Success -> {
                    val json = Parser().parse(StringBuilder(result.value)) as JsonObject
                    val heading = json.string("Heading")
                    val abstract = json.string("AbstractText")
                    val abstractUrl = json.string("AbstractURL")
                    val answer = json.string("Answer")
                    val definition = json.string("Definition")
                    val definitionUrl = json.string("DefinitionURL")
                    val redirect = json.string("Redirect")
                    val firstResult = {
                        val results = json.array<JsonObject>("Results")
                        if (results == null || results.isEmpty()) {
                            val related = json.array<JsonObject>("RelatedTopics")
                            if (related == null || related.isEmpty()) null
                            else related.first()
                        } else results.first()
                    }()
                    val firstText = firstResult?.string("Text")
                    val firstUrl = firstResult?.string("FirstURL")
                    val compilation = DuckResult(
                            heading, abstract, abstractUrl, answer,
                            definition, definitionUrl, redirect,
                            firstText, firstUrl
                    )
                    success(compilation)
                }
            }
        }
    }
}
