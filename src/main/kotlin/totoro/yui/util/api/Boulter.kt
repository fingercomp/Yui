package totoro.yui.util.api

import org.jsoup.Jsoup

object Boulter {
    private const val useragent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) " +
            "Chrome/59.0.3071.115 Safari/537.36"

    fun anagrams(letters: String): List<String> {
        return try {
            val doc = Jsoup.connect("http://boulter.com/anagram/?letters=$letters").userAgent(useragent).get()
            doc.select("a").map { it.text() }
        } catch (e: Exception) {
            listOf()
        }
    }
}
