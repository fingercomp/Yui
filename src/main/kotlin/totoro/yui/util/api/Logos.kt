package totoro.yui.util.api

import org.jsoup.Jsoup
import totoro.yui.Log
import totoro.yui.util.api.data.Link

object Logos {
    private const val useragent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) " +
            "Chrome/59.0.3071.115 Safari/537.36"

    private var initialized = false
    private var cache = mutableListOf<Link>()

    fun uk(request: String): List<Link> {
        if (!initialized) initialize()
        return if (cache.isEmpty()) listOf()
        else {
            cache.filter { it.title.toLowerCase().contains(request) }
        }
    }

    private fun initialize() {
        try {
            val doc = Jsoup.connect("http://logos-pravo.ru/ugolovnyy-kodeks-rf-uk-rf").userAgent(useragent).get()
            doc.select("a").forEach {
                if (it.text().startsWith("Статья")) {
                    val title = it.text()
                    val url = "http://logos-pravo.ru/" + it.attr("href")
                    cache.add(Link(title, url))
                }
            }
        } catch (e: Exception) {
            Log.error("Cannot initialize the list of UK RF articles!")
        }
    }
}
