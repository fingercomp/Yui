package totoro.yui.util.api

import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import com.github.kittinunf.fuel.httpGet
import com.github.kittinunf.result.Result
import totoro.yui.util.api.data.Geolocation

object Geo {
    fun locationByHost(ip: String, handler: (Geolocation?) -> Unit) {
        "http://ip-api.com/json/$ip".httpGet().responseString { _, _, result ->
            when (result) {
                is Result.Failure -> handler(null)
                is Result.Success -> {
                    val json = Parser().parse(StringBuilder(result.value)) as JsonObject
                    val country = json.string("country")
                    val city = json.string("city")
                    val provider = json.string("as")
                    val timezone = json.string("timezone")
                    handler(Geolocation(country, city, provider, timezone))
                }
            }
        }
    }
}
