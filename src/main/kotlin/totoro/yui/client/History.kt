package totoro.yui.client

import totoro.yui.util.LimitedList

/**
 * Stores some amount of last chat messages for actions to use them.
 */

@Suppress("unused")
class History(val size: Int) {
    class Record(val user: String, val message: String)

    private val history = HashMap<String, LimitedList<Record>>()

    fun add(chan: String, user: String, message: String) {
        if (!history.contains(chan)) history[chan] = LimitedList(size)
        history[chan]?.addLast(Record(user, message))
    }

    fun size(chan: String): Int = history[chan]?.size ?: 0

    fun get(chan: String, index: Int) = history[chan]?.getOrNull(index)

    fun getFromEnd(chan: String, index: Int) = history[chan]?.getOrNull((history[chan]?.size ?: 1) - 1 - index)

    fun last(chan: String) = history[chan]?.lastOrNull()

    fun lastByUser(chan: String, user: String) = history[chan]?.lastOrNull { it.user == user }
}
