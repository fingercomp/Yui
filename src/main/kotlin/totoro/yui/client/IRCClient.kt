package totoro.yui.client

import net.engio.mbassy.listener.Handler
import org.kitteh.irc.client.library.Client
import org.kitteh.irc.client.library.element.User
import org.kitteh.irc.client.library.event.channel.ChannelMessageEvent
import org.kitteh.irc.client.library.event.channel.UnexpectedChannelLeaveViaKickEvent
import org.kitteh.irc.client.library.event.channel.UnexpectedChannelLeaveViaPartEvent
import org.kitteh.irc.client.library.event.client.ClientConnectionClosedEvent
import org.kitteh.irc.client.library.event.client.ClientConnectionEstablishedEvent
import org.kitteh.irc.client.library.event.client.ClientNegotiationCompleteEvent
import org.kitteh.irc.client.library.event.client.ClientReceiveMotdEvent
import org.kitteh.irc.client.library.event.user.PrivateMessageEvent
import totoro.yui.Config
import totoro.yui.Log
import totoro.yui.actions.CommandAction
import totoro.yui.actions.MessageAction
import totoro.yui.util.Dict


@Suppress("unused", "UNUSED_PARAMETER", "MemberVisibilityCanBePrivate")
class IRCClient {
    private val coolNicknames = Dict.of("yui`", "yuki`", "yumi`", "ayumi`")

    private val nick = coolNicknames()
    private val realname = "Yui the Bot"

    val history = History(100)
    private val client = Client.builder()
            .nick(nick)
            .user(nick)
            .name(nick)
            .realName(realname)
            .serverHost(Config.host)
            .build()

    private val commandActions = ArrayList<CommandAction>()
    private val messageActions = ArrayList<MessageAction>()

    private var onceConnected: (() -> Unit)? = null

    init {
        Config.chan.forEach { client.addChannel(it) }
        client.eventManager.registerEventListener(this)
    }


    fun connect() {
        client.connect()
    }

    fun login(pass: String?) {
        if (pass != null) {
            client.sendMessage("nickserv", "identify $pass")
            Log.info("Logged in.")
        }
    }

    fun broadcast(message: String) {
        Config.chan.forEach { client.sendMessage(it, message) }
        Log.outgoing(message)
    }

    fun send(chan: String, message: String) {
        client.sendMessage(chan, message)
        Log.outgoing("[$chan] $message")
    }

    fun sendMultiline(chan: String, messages: List<String>) {
        messages.filter { it.isNotEmpty() }.map {
            send(chan, it)
        }
    }

    fun sendMultiline(chan: String, message: String) {
        sendMultiline(chan, message.split("\n", "\r"))
    }

    fun start(onceConnected: () -> Unit) {
        connect()
        this.onceConnected = onceConnected
    }


    fun registerCommandAction(action: CommandAction) {
        for ((index, value) in commandActions.withIndex()) {
            if (value.priority.ordinal >= action.priority.ordinal) {
                commandActions.add(index, action)
                break
            }
        }
        commandActions.add(action)
    }

    fun registerMessageAction(action: MessageAction) {
        messageActions.add(action)
    }


    fun getListOfUsersOnline(): List<User> =
            client.channels.flatMap { it.users }

    fun getListOfNicknamesOnline(): List<String> =
            client.channels.flatMap { it.nicknames }

    fun isUserOnline(nickname: String): Boolean =
            getListOfNicknamesOnline().any { it == nickname }

    fun isBroteOnline(): Boolean =
            isUserOnline("brote")

    fun getUserHost(nickname: String): String? =
            getListOfUsersOnline().firstOrNull { it.nick == nickname }?.host


    private fun processMessage(chan: String, user: String, message: String) {
        Log.incoming("[${ if (chan == user) "PM" else chan }] $user: $message")
        if (!tryActionProcessors(chan, user, message))
            history.add(chan, user, message)
    }
    private fun tryActionProcessors(chan: String, user: String, message: String): Boolean {
        val command = Command.parse(message, chan, user, nick)
        if (command != null) {
            // if the command was parsed successfully we will try to process it via command processors
            if (Config.blackUsers.contains(user))
                send(chan, "$user: totoro says you are baka " + Dict.Offended())
            else {
                // check commands blacklist
                if (command.name in Config.blackCommands && user !in Config.admins)
                    send(chan, "totoro says - don't use the ~${command.name} command " + Dict.Upset())
                else {
                    // call registered action processors
                    @Suppress("LoopToCallChain")
                    for (action in commandActions) {
                        // command can be consumed by one of processors
                        // in this case we do not need to try the rest of actions
                        if (action.processCommand(this, command) == null) return true
                    }
                }
            }
        } else {
            // if the message cannot be interpreted as a correct command then we will try common message processors
            for (action in messageActions) {
                if (action.processMessage(this, chan, user, message) == null) return false
            }
        }
        return false
    }


    @Handler
    fun motd(event: ClientReceiveMotdEvent) {
        if (onceConnected != null) {
            onceConnected?.invoke()
            onceConnected = null
        }
    }

    @Handler
    fun meow(event: ClientNegotiationCompleteEvent) =
        Log.info("I'm ready to chat (today I'm $nick)!")

    @Handler
    fun ready(event: ClientConnectionEstablishedEvent) =
        Log.info("I'm connected!")

    @Handler
    fun kawaii(event: ClientConnectionClosedEvent) =
        Log.info("I'm disconnected! ${Dict.Upset()}")

    @Handler
    fun baka(event: UnexpectedChannelLeaveViaKickEvent) = {
        Log.info("I was kicked! ${Dict.Offended}")
        event.channel.join()
    }

    @Handler
    fun baka(event: UnexpectedChannelLeaveViaPartEvent) = {
        Log.info("I did leave, but I do not remember why...")
        event.channel.join()
    }

    @Handler
    fun incoming(event: ChannelMessageEvent) {
        processMessage(event.channel.name, event.actor.nick, event.message)
    }

    @Handler
    fun private(event: PrivateMessageEvent) {
        if (Config.pm) {
            processMessage(event.actor.nick, event.actor.nick, event.message)
        }
    }
}
