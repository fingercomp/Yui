package totoro.yui.actions

/**
 * This parameter indicates in which order action processors will be called.
 */

enum class Priority {
    PROXIMATE, INTERMEDIATE, PENULTIMATE, ULTIMATE
}
