package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.F
import totoro.yui.util.api.Logos

@Action
class LawAction : SensitivityAction("uk", "law", "criminal") {

    private val maxArticles = 4

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        val articles = Logos.uk(command.content)
        if (articles.isEmpty())
            client.send(command.chan, F.Gray + "the law voice is silent" + F.Reset)
        else {
            articles.take(maxArticles).forEach {
                client.send(command.chan, F.Yellow + it.title + F.Reset + " / " + it.url)
            }
            if (articles.size > maxArticles) {
                client.send(command.chan, "(and ${articles.size - maxArticles} more...)")
            }
        }
        return true
    }
}
