package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import java.nio.charset.StandardCharsets
import java.util.*

@Action
class Base64Action : SensitivityAction("base64", "base", "64", "b64") {
    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        if (command.args.firstOrNull() == "encode") {
            client.send(
                    command.chan,
                    Base64.getEncoder().encodeToString(
                            command.content.drop(6).trim().toByteArray(StandardCharsets.UTF_8)
                    )
            )
        } else if (command.args.firstOrNull() == "decode") {
            client.send(
                    command.chan,
                    Base64.getDecoder().decode(
                            command.content.drop(6).trim()
                    ).toString(StandardCharsets.UTF_8)
            )
        } else {
            client.send(command.chan, "do you want to `~base64 decode xxx` or `~base64 encode xxx`?")
        }
        return true
    }
}
