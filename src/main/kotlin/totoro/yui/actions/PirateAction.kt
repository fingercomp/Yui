package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.Dict

@Action
class PirateAction : SensitivityAction("pirate") {

    private val appeals = Dict.of(
            "arrr!", "yo-ho-ho!", "yo-ho-ho, and a bottle of rum!",
            "new and russian", "ahoy!", "where's my parrot?",
            "fire in the hole!", "release the kraken!", "all hands on deck!",
            "dead men tell no tales!", "feed the fishes!", "land ho!",
            "sail ho!", "shiver me timbers!")

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        // try to piratify nickname
        val name = command.args.firstOrNull()
        val text = if (name?.contains('r') == true)
        // search for 'r'
            name.replace("r", "rrr") + "!"
        else
        // or just be a pirate
            appeals()
        client.send(command.chan, text)
        return true
    }
}
