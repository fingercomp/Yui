package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.Dict

@Action
class UnsureAction: CommandAction {
    /**
     * This action must be always the last one in the processing queue
     */
    override val priority = Priority.ULTIMATE

    override fun processCommand(client: IRCClient, command: Command): Command? {
        client.send(command.chan, Dict.NotSure())
        return null
    }
}
