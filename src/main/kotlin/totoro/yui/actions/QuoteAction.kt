package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.db.Database
import totoro.yui.db.Quote
import totoro.yui.util.Dict
import totoro.yui.util.F
import totoro.yui.util.LimitedHashMap
import totoro.yui.util.LimitedList

class QuoteAction(private val database: Database) : SensitivityAction("q", "quote", "mq", "multiquote", "cancel"), MessageAction {

    companion object {
        private var action: QuoteAction? = null
        fun instance(database: Database): QuoteAction {
            if (action == null) action = QuoteAction(database)
            return action!!
        }
    }

    private val unfinishedQuotesLimit = 10
    private val quoteLengthLimit = 100
    private val outputLinesLimit = 5

    private val unfinished = LimitedHashMap<String, LimitedList<String>>(unfinishedQuotesLimit)

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        if (command.name == "q" || command.name == "quote") {
            // read quote by given ID, or read a random quote
            val id = command.args.firstOrNull()?.let { arg ->
                if (arg.startsWith("#")) arg.drop(1)
                else arg
            }
            val quote = id?.toIntOrNull()?.let {
                database.quotes?.get(it.toLong())
            } ?: database.quotes?.random()
            if (quote != null) {
                val numberOfLines = quote.text.count { it == '\n' } + 1
                val id = quote.id.toString()
                var lines = quote.text.split('\n').toMutableList()
                if (numberOfLines > 1) {
                    val tab = " ".repeat((id.length + 3))
                    lines = lines.mapIndexed { index, s -> if (index > 0) tab + s else s }.toMutableList()
                    if (numberOfLines > outputLinesLimit) {
                        lines = (lines.take(outputLinesLimit - 1) +
                                (tab + "... (https://quotes.fomalhaut.me/quote/${quote.id})")).toMutableList()
                    }
                }
                lines[0] = "${F.Yellow}#$id${F.Reset}: ${lines[0]}"
                client.sendMultiline(command.chan, lines)
            } else {
                client.send(command.chan, "no quotes today " + Dict.Kawaii())
            }
        } else if (command.name == "cancel") {
            if (unfinished.containsKey(command.user)) {
                // cancel the creation of this this multiline quote
                unfinished.remove(command.user)
                client.send(command.chan, "no problem")
            } else {
                client.send(command.chan, "there is nothing to cancel")
            }
        } else {
            // process multi-line quotes creation requests
            if (unfinished.containsKey(command.user)) {
                // finish this multiline quote
                save(client, command.chan, Quote(0, unfinished[command.user]!!.joinToString("\n")))
                unfinished.remove(command.user)
            } else {
                // begin a new multiline quote
                unfinished[command.user] = LimitedList(quoteLengthLimit)
                client.send(command.chan, "enter your quote line by line and then finish it with another ~mq command (or just ~cancel)")
            }
        }
        return true
    }

    override fun processMessage(client: IRCClient, channel: String, user: String, message: String): String? {
        // check if there are any unfinished quotes
        return if (unfinished.size > 0 && unfinished.containsKey(user)) {
            // and then add a new line to the quote
            unfinished[user]!!.add(message)
            null
        } else message
    }

    private fun save(client: IRCClient, channel: String, quote: Quote) {
        if (quote.text.isNotBlank()) {
            val index = database.quotes?.add(quote)
            val text = index?.let { "i'll remember this" } ?: "something went wrong with my storage "+Dict.Upset()
            client.send(channel, text)
        } else {
            client.send(channel, "you really need to spice up this quote")
        }
    }
}
