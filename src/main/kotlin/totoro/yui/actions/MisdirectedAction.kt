package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient

@Action
class MisdirectedAction : SensitivityAction("python", "wa", "tr", "tell") {

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        client.send(command.chan, "maybe try .${command.name} (with the dot)")
        return true
    }
}
