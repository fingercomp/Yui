package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.F
import totoro.yui.util.api.Geo

@Action
class GeoAction : SensitivityAction("geo", "location", "geolocation", "loc", "locate",
        "divanon", "deanon", "where", "whereis") {

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        val hostname = when {
            command.content.isEmpty() -> client.getUserHost(command.user)
            client.isUserOnline(command.content) -> client.getUserHost(command.content)
            else -> command.content
        }
        if (hostname != null) {
            Geo.locationByHost(hostname) {
                if (it != null) {
                    val location =
                        if (it.country != null && it.city != null) "${it.city}, ${it.country}"
                        else it.country ?: (it.city ?: "-")
                    val provider = if (it.provider != null) "(${it.provider})" else ""
                    client.send(command.chan, F.Yellow + location + F.Reset + " $provider")
                } else {
                    client.send(command.chan, "cannot retrieve the data for given hostname")
                }
            }
        } else {
            client.send(command.chan, "cannot retrieve hostname for the user")
        }
        return true
    }
}
