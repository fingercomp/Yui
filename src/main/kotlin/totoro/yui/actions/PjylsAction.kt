package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.Dict

@Action
class PjylsAction : SensitivityAction("zog", "pjyls") {

    private val zog = Dict.of("　ム\nム　ム", " ▴\n▴ ▴")

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        client.sendMultiline(command.chan, zog())
        return true
    }
}
