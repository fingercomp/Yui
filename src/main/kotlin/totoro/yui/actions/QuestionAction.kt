package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.Dict

@Action
class QuestionAction : CommandAction {
    /**
     * This action must be processed close to the end, but not the last
     */
    override val priority = Priority.PENULTIMATE

    private val sensitivities = listOf("ball", "?", "8", "8?")
    private val dict = Dict.Yeah + Dict.Nope + Dict.Maybe

    override fun processCommand(client: IRCClient, command: Command): Command? {
        return if (sensitivities.contains(command.name) || command.content.endsWith('?')) {
            client.send(command.chan, dict())
            null
        } else command
    }
}
