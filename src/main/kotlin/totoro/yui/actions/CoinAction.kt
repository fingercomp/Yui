package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.F
import totoro.yui.util.api.CryptoCompare
import java.text.DecimalFormat
import java.time.Duration

@Action
class CoinAction : SensitivityAction("coin", "cur", "currency",
        "btc", "bitcoin", "eth", "ether", "ethereum", "fcn", "fantom", "fantomcoin",
        "doge", "dogecoin", "neo", "neocoin", "monero", "xmr", "ripple") {

    private val longFormat = DecimalFormat("0.#####################")
    private val shortFormat = DecimalFormat("0.###")

    private fun getCurrencyCodes(args: List<String>): List<String> =
            args.filter { !it[0].isDigit() && it[0] != 'p' }.map { it.toUpperCase() }

    private fun getDelta(args: List<String>): String? =
            args.firstOrNull { it[0] == 'p' }

    private fun getAmount(args: List<String>): Double? =
            args.mapNotNull { it.toDoubleOrNull() }.firstOrNull()

    private fun formatDelta(delta: Double): String {
        return "(" + when {
            delta > 0 -> F.Green + "▴" + F.Reset
            delta < 0 -> F.Red + "▾" + F.Reset
            else -> ""
        } + shortFormat.format(Math.abs(delta)) + "%)"
    }

    override fun handleCommand(client: IRCClient, command: Command): Boolean {

        fun get(from: String, to: String, range: String?, amount: Double?) {
            val duration = if (range != null)
                try { Duration.parse(range.toUpperCase()) } catch (e: Exception) { null }
            else null

            if (range != null && duration == null) {
                client.send(command.chan, F.Gray + "try ISO 8601 for time duration" + F.Reset)
            } else {
                val finalAmount = amount ?: 1.0
                CryptoCompare.get(
                        from.toUpperCase(),
                        to.toUpperCase(),
                        duration,
                        { currency ->
                            client.send(command.chan,
                                    "$finalAmount ${from.toUpperCase()} -> " +
                                    F.Yellow + longFormat.format(currency.price * finalAmount) + F.Reset + " ${currency.code}" +
                                    if (currency.description != null) F.Italic + " (${currency.description})" + F.Reset else "" +
                                    if (range != null) "  " + formatDelta(currency.delta) else ""
                            )
                        },
                        {
                            client.send(command.chan, F.Gray + "cannot get the rate for this" + F.Reset)
                        }
                )
            }
        }

        val codes = getCurrencyCodes(command.args)
        val delta = getDelta(command.args)
        val amount = getAmount(command.args)

        val from = when (command.name) {
            "btc", "bitcoin" -> "BTC"
            "eth", "ether", "ethereum" -> "ETH"
            "doge", "dogecoin" -> "DOGE"
            "neo", "neocoin" -> "NEO"
            "monero", "xmr" -> "XMR"
            "ripple" -> "XRP"
            "fcn", "fantom", "fantomcoin" -> "FCN"
            else -> if (codes.isNotEmpty()) codes.first() else "USD"
        }

        val to = when {
            codes.isEmpty() || codes.last() == from -> if (from == "RUB" || from == "BTC") "USD" else "RUB"
            else -> codes.last()
        }

        get(from, to, delta, amount)

        return true
    }
}
