package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient

@Action
class ViceCityAction : SensitivityAction("vc") {
    private val substitutions = hashMapOf(
            'а' to 'a', 'б' to '6', 'в' to 'в', 'г' to 'r', 'д' to 'д', 'е' to 'e', 'ё' to 'e', 'ж' to 'ж',
            'з' to 'з', 'и' to 'и', 'й' to 'и', 'к' to 'k', 'л' to 'л', 'м' to 'm', 'н' to 'н', 'о' to 'o',
            'п' to 'n', 'р' to 'p', 'с' to 'c', 'т' to 'т', 'у' to 'y', 'ф' to 'ф', 'х' to 'x', 'ц' to 'u',
            'ч' to 'ч', 'ш' to 'w', 'щ' to 'w', 'э' to 'э', 'ю' to 'ю'
    )

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        client.send(command.chan, command.content.trim().toLowerCase().map {
            if (substitutions.containsKey(it)) { substitutions[it] } else { it }
        }.joinToString(""))
        return true
    }
}
