package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.Dict

class SimpleAction(activators: List<String>, private val response: Dict<String>) : SensitivityAction(activators) {
    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        client.send(command.chan, response())
        return true
    }
}
