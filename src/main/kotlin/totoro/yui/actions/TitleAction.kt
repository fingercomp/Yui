package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.F
import totoro.yui.util.api.Title

/**
 * Searches for URL adresses in the message and then prints the titles
 * of corresponding web pages
 */

@Action
class TitleAction : SensitivityAction("title"), MessageAction {

    companion object {
        val instance: TitleAction by lazy { TitleAction() }
    }

    private val urlRegex = ".*https?://.*\\..*".toRegex()

    private fun printAllTitles(phrases: List<String>, channel: String, client: IRCClient): Boolean {
        val titles = phrases.filter { it.matches(urlRegex) }.mapNotNull { Title.get(it) }
        titles.forEach { client.send(channel, "${F.Yellow}[ $it ]${F.Reset}") }
        return titles.isNotEmpty()
    }

    override fun processMessage(client: IRCClient, channel: String, user: String, message: String): String? {
        return if (!client.isBroteOnline() && message.contains("http")) {
            if (printAllTitles(message.split(' '), channel, client)) null
            else message
        } else message
    }

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        when {
            command.args.size == 1 && command.args.first().startsWith('^') -> {
                val index = if (command.args.first() == "^") 1
                else command.args.first().drop(1).toIntOrNull()

                if (index != null && index <= client.history.size) {
                    val record = client.history.getFromEnd(command.chan, index - 1)
                    if (record != null) {
                        if (!printAllTitles(record.message.split(" "), command.chan, client))
                            client.send(command.chan, "i don't see any URLs there")
                    } else {
                        client.send(command.chan, "something went wrong and I'm not able to recall this message")
                    }
                } else {
                    client.send(command.chan, "use ^n format (where 1 <= n <= ${client.history.size(command.chan)}) to point to a nth message back, like ^2")
                }
            }
            command.args.isNotEmpty() ->
                printAllTitles(command.args, command.chan, client)
        }
        return true
    }
}
