package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.Dict
import totoro.yui.util.F
import totoro.yui.util.api.DuckDuckGo

@Action
class DuckAction : SensitivityAction("d", "duck", "duckduck", "duckduckgo", "dd", "ddg", "go", "instant", "answer") {

    private val responses = Dict.of("nope", "nothing", "empty")

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        val result = DuckDuckGo.search(command.content, { result ->
            if (available(result.answer)) {
                client.send(command.chan, F.Yellow + result.answer + F.Reset)
            } else if (available(result.definition)) {
                client.send(command.chan, F.Italic + result.definition + F.Reset)
                if (available(result.definitionUrl))
                    client.send(command.chan, "(${result.definitionUrl})")
            } else if (available(result.firstUrl )) {
                when {
                    available(result.firstText) ->
                        client.send(command.chan, F.Yellow + result.firstText + F.Reset + " / ${result.firstUrl}")
                    available(result.heading) ->
                        client.send(command.chan, F.Yellow + result.heading + F.Reset + " / ${result.firstUrl}")
                    else ->
                        client.send(command.chan, result.firstUrl!!)
                }
            } else if (available(result.abstract)) {
                client.send(command.chan, F.Italic + result.abstract + F.Reset)
                if (available(result.abstractUrl))
                    client.send(command.chan, "(${result.abstractUrl})")
            } else if (available(result.redirect)) {
                client.send(command.chan, result.redirect!!)
            } else {
                client.send(command.chan, responses() + " (keep in mind - this is not a full search, but an Instant Answers API)")
            }
        }, {
            client.send(command.chan, responses() + " (keep in mind - this is not a full search, but an Instant Answers API)")
        })
        return true
    }

    private fun available(value: String?): Boolean = value != null && value.isNotBlank()
}
