package totoro.yui.actions

import totoro.yui.Yui
import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.F
import java.io.BufferedReader
import java.io.File
import java.io.FileInputStream
import java.io.InputStreamReader

@Action
class CitationAction : SensitivityAction("cite", "citation") {

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        val citation = getCitation()
        if (citation == null)
            client.send(command.chan, "${F.Gray}nothing to cite here${F.Reset}")
        else
            citation.split('\n').map { client.send(command.chan, "${F.Yellow}$it${F.Reset}") }
        return true
    }

    private fun getCitation(): String? {
        val booksDir = File("./books")
        if (booksDir.exists()) {
            val files = booksDir.listFiles()
            if (files.isNotEmpty()) {
                val file = files[Yui.Random.nextInt(files.size)]
                val position = Math.abs(Yui.Random.nextLong()) % file.length()
                return readPhrase(file, position) ?: readPhrase(file, 0)
            }
        }
        return null
    }

    private fun readPhrase(file: File, position: Long): String? {
        val stream = FileInputStream(file)
        val reader = BufferedReader(InputStreamReader(stream, "UTF-8"))
        if (position > 0) {
            reader.skip(position)
            // kinda crutchy solution, but will work as long as we need to check only two last letters
            var secondToLast: Char? = null
            var last: Char? = null
            while (reader.ready()) {
                val char = reader.read().toChar()
                if (char == '.' && !(last == 'r' && secondToLast == 'M')) break
                secondToLast = last
                last = char
            }
        }
        val builder = StringBuilder()
        while (reader.ready()) {
            val char = reader.read().toChar()
            if (!char.isWhitespace()) {
                builder.append(char)
                break
            }
        }
        while (reader.ready()) {
            val char = reader.read().toChar()
            if (char != '\n') builder.append(char)
            if ((char == '.' && !builder.endsWith("Mr.")) || char == '\n') break
        }
        reader.close()
        stream.close()
        return if (builder.isNotEmpty() && builder.isNotBlank()) builder.toString()
        else null
    }
}
