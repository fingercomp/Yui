package totoro.yui.actions

import totoro.yui.Yui
import totoro.yui.client.Command
import totoro.yui.client.IRCClient

@Action
class ZalgoAction : SensitivityAction("zalgo", "creepy", "horror", "glitch", "glitchy") {
    private val diacritics = "̴̵̶̷̸̡̢̧̨̛̖̗̘̙̜̝̞̟̠̣̤̥̦̩̪̫̬̭̮̯̰̱̲̳̹̺̻̼͇͈͉͍͎̀́̂̃̄̅̆̇̈̉̊̋̌̍̎̏̐̑̒̓̔̽̾̿̀́͂̓̈́͆͊͋͌̕̚ͅ͏͓͔͕͖͙͚͐͑͒͗͛ͣͤͥͦͧͨͩͪͫͬͭͮͯ͘͜͟͢͝͞͠͡"
    private val level = 4

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        val builder = StringBuilder()
        command.content.forEach {
            builder.append(it)
            if (!it.isWhitespace()) {
                val num = Yui.Random.nextInt(level + 1)
                for (i in 0..num) {
                    builder.append(diacritics[Yui.Random.nextInt(diacritics.length)])
                }
            }
        }
        client.send(command.chan, builder.toString())
        return true
    }
}
