package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.Language
import totoro.yui.util.LanguageHelper

@Action
class TranslitAction : SensitivityAction("tt",
        "trans", "translit", "transliterate", "transliteration") {

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        // phrase will be taken from history, if no arguments were given,
        // or the only argument is the nickname of some user
        // otherwise the content of the command will be taken as the phrase
        val phrase = if (command.args.size > 1 || (command.args.size == 1 && !client.isUserOnline(command.args.first())))
            command.content
        else {
            val nickname = if (command.args.isNotEmpty()) command.args.first() else command.user
            client.history.lastByUser(command.chan, nickname)?.message
        }
        val text = phrase?.let { transliterate(client, it) } ?: "what do i need to tt?"
        client.send(command.chan, text)
        return true
    }

    private val straight = hashMapOf(
            'a' to 'ф', 'b' to 'и', 'c' to 'с', 'd' to 'в', 'e' to 'у', 'f' to 'а', 'g' to 'п', 'h' to 'р',
            'i' to 'ш', 'j' to 'о', 'k' to 'л', 'l' to 'д', 'm' to 'ь', 'n' to 'т', 'o' to 'щ', 'p' to 'з',
            'q' to 'й', 'r' to 'к', 's' to 'ы', 't' to 'е', 'u' to 'г', 'v' to 'м', 'w' to 'ц', 'x' to 'ч',
            'y' to 'н', 'z' to 'я',
            '`' to 'ё', '[' to 'х', ']' to 'ъ', ';' to 'ж', '\'' to 'э', ',' to 'б', '.' to 'ю',
            '~' to 'Ё', '{' to 'Х', '}' to 'Ъ', ':' to 'Ж', '"' to 'Э', '<' to 'Б', '>' to 'Ю',
            '/' to '.', '?' to ',', '@' to '"', '#' to '№', '$' to ';', '^' to ':', '&' to '?'
    )
    private val reversed = straight.entries.associateBy({ it.value }) { it.key }

    private fun transliterate(client: IRCClient, phrase: String): String {
        if (phrase.isEmpty()) return phrase
        // calculate positions of all user nicknames in the given phrase
        val users = client.getListOfNicknamesOnline()
        val excluded = users.mapNotNull {
            val beginning = phrase.indexOf(it)
            if (beginning >= 0) Pair(beginning, it.length)
            else null
        }
        // detect which way we need to transliterate this string
        val rules = if (LanguageHelper.detect(phrase, excluded) == Language.ENGLISH) straight else reversed
        // convert
        return phrase.mapIndexed { index, it ->
            when {
                excluded.any { it.first <= index && it.first + it.second > index } -> it
                it in rules -> rules[it]!!
                it.toLowerCase() in rules -> rules[it.toLowerCase()]!!.toUpperCase()
                else -> it
            }
        }.joinToString("")
    }
}
