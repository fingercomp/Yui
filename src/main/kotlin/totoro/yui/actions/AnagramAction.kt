package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.F
import totoro.yui.util.RandomIterator
import totoro.yui.util.api.Boulter

@Action
class AnagramAction : SensitivityAction("anagram", "anagrams") {

    private val maxAnagrams = 10
    private val maxInputLength = 50

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        return if (command.content.isNotBlank()) {
            if (command.content.length > maxInputLength)
                client.send(command.chan, F.Gray + "let's not do this" + F.Reset)
            else {
                val rhymes = Boulter.anagrams(command.content)
                if (rhymes.isNotEmpty()) {
                    client.send(command.chan, F.Yellow + command.content + F.Reset + ": " +
                            RandomIterator(rhymes).take(maxAnagrams).joinToString(", "))
                } else client.send(command.chan, F.Gray + "no anagrams found" + F.Reset)
            }
            true
        } else false
    }
}
