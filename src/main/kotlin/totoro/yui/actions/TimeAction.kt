package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.api.Geo
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

@Action
class TimeAction : SensitivityAction("t", "time", "date") {

    private val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS")

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        val hostname = when {
            command.content.isEmpty() -> client.getUserHost(command.user)
            command.args.first() == "server" -> null
            client.isUserOnline(command.content) -> client.getUserHost(command.content)
            else -> command.content
        }
        if (hostname == null) {
            client.send(command.chan, LocalDateTime.now().format(formatter))
        } else {
            Geo.locationByHost(hostname) {
                if (it != null) {
                    try {
                        val zone = ZoneId.of(it.timezone)
                        val time = ZonedDateTime.now(zone)
                        client.send(command.chan, time.toLocalDateTime().format(formatter))
                    } catch (_: Exception) {
                        client.send(command.chan, "my time and space continuum is completely messed up")
                    }
                } else {
                    client.send(command.chan, "cannot retrieve the timezone info")
                }
            }
        }
        return true
    }
}
