package totoro.yui.actions

import totoro.yui.client.Command
import totoro.yui.client.IRCClient
import totoro.yui.util.F
import totoro.yui.util.api.Geo
import totoro.yui.util.api.Wttr

@Action
class WeatherAction : SensitivityAction("weather", "wttr") {

    override fun handleCommand(client: IRCClient, command: Command): Boolean {
        if (command.content.isEmpty())
            client.getUserHost(command.user)?.let { host ->
                Geo.locationByHost(host) {
                    showWeather("${it?.city ?: ""} ${it?.country ?: ""}", client, command)
                }
            } ?: showWeather("", client, command)
        else showWeather(command.content, client, command)
        return true
    }

    private fun showWeather(location: String, client: IRCClient, command: Command) {
        Wttr.get(location, { weather ->
            client.send(command.chan, "${weather.location}: ${F.Yellow}${weather.condition} / ${weather.temperature} / " +
                    "${weather.wind} / ${weather.visibility} / ${weather.precipitation}${F.Reset}")
        }, {
            client.send(command.chan, "${F.Gray}the weather is unclear${F.Reset}")
        })
    }
}
